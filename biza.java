import java.util.Scanner;
public class biza {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        boolean WrongInput = false;
        /*This boolean is used to control the while loops for each switch statement, to break the loop when a valid
        number is entered.*/
        int InputNumber;
        /*The integer InputNumber is used to get the number entered by the "customer" as to pick what pizza/size/topping
        is chosen.*/
        double Sum = 0;
        /*The sum is used to calculate the total cost of the order, the sum is updated in each loop.*/
        String Pizza = "";
        String Size = "";
        String Topping = "";
        /*These three strings are used to catch the name of the pizza, its size and what topping(if any) was ordered.*/
        System.out.print("hello this is papa johns, this is the menu:\n");
        System.out.printf("1. Chicago 65,- %21s","6. California 70,-");
        System.out.printf("\n2. Sicilian 65,- %17s","7. Maltese 70,-");
        System.out.printf("\n3. Greek 65,- %19s","8. Milano 85,-");
        System.out.printf("\n4. Magheritha 65,- %10s","9. Viking 85,-");
        System.out.printf("\n5. Hawaii 65,- %21s","10. Casanova 95,-");

        InputNumber = in.nextInt();
        while(!WrongInput) {
            switch (InputNumber) {
                case 1:
                    Pizza = "Chicago";
                    Sum += 65;
                    WrongInput=true;
                    break;
                case 2:
                    Pizza = "Sicilian";
                    Sum += 65;
                    WrongInput=true;
                    break;
                case 3:
                    Pizza = "Greek";
                    Sum += 65;
                    WrongInput=true;
                    break;
                case 4:
                    Pizza = "Magheritha";
                    Sum += 65;
                    WrongInput=true;
                    break;
                case 5:
                    Pizza = "Hawaii";
                    Sum += 65;
                    WrongInput=true;
                    break;
                case 6:
                    Pizza = "California";
                    Sum += 70;
                    WrongInput=true;
                    break;
                case 7:
                    Pizza = "Maltese";
                    Sum += 70;
                    WrongInput=true;
                    break;
                case 8:
                    Pizza = "Milano";
                    Sum += 85;
                    WrongInput=true;
                    break;
                case 9:
                    Pizza = "Viking";
                    Sum += 85;
                    WrongInput=true;
                    break;
                case 10:
                    Pizza = "Casanova";
                    Sum += 95;
                    WrongInput=true;
                    break;
                default:
                    System.out.println("We dont have that pizza, please choose a pizza between 1 to 10.");
                    InputNumber = in.nextInt();
            }

        }
        /*This first loop catches which pizza has been ordered by the customer, and if an invalid input is seen
        the loop does not break and simply asks for a valid input.*/
        System.out.println("You ordered a: "+Pizza+ " \nWhat size do you want?");

        InputNumber = in.nextInt();
        while(WrongInput) {
            switch (InputNumber) {
                case 1:
                    Size = "child";
                    Sum = Sum * 0.75;
                    WrongInput=false;
                    break;
                case 2:
                    Size = "standard";
                    WrongInput=false;
                    break;
                case 3:
                    Size = "family";
                    Sum = Sum * 1.5;
                    WrongInput=false;
                    break;
                default:
                    System.out.println("Invalid pizza size, please choose a size between 1 to 3.");
                    InputNumber = in.nextInt();
                        /*The boolean can be seen here being switched back and forth from false to true,
                        this is simply a way of breaking the loop a valid number is input*/
            }
        }
        /*The second loop asks the customer for the size of the pizza and calculates the price accordingly*/

        System.out.println("You ordered a "+Size+" sized "+Pizza+" pizza." +
                "\nDo you want to add any extra toppings? Press 0 to finish."+
                "\nThe choices are: "+"\n1. Clam 10,-\n2. Pepperoni 10,-\n3. Extra Cheese 10,-" +
                "\n4. Tuna 15,-\n5. Bacon 15,-");

        InputNumber = in.nextInt();
        while(!WrongInput) {
            switch (InputNumber) {
                case 1:
                    Topping = "Clam";
                    Sum += 10;
                    WrongInput=true;
                    break;
                case 2:
                    Topping = "Pepperoni";
                    Sum += 10;
                    WrongInput=true;
                    break;
                case 3:
                    Topping = "Extra Cheese";
                    Sum += 10;
                    WrongInput=true;
                    break;
                case 4:
                    Topping = "Tuna";
                    Sum += 15;
                    WrongInput=true;
                    break;
                case 5:
                    Topping = "Bacon";
                    Sum += 15;
                    WrongInput=true;
                    break;
                case 0:
                    WrongInput=true;
                    break;
                default:
                    System.out.println("Invalid input, press 0 to finish or 1-5 to pick a topping.");
                    InputNumber = in.nextInt();
            }
        }
        /*The third while loop does the same thing as the previous 2 but asks the user for toppings if any.
        It does not contain the default case to catch invalid input, as choosing a number between 1 to 5 is
        not a requirement.*/

        if(InputNumber==0){
            System.out.println("You ordered a "+Size+" sized "+Pizza+" and no toppings.\n" +
                    "The total price is "+Sum+",-");
        }else
            System.out.println("You ordered a "+Size+" sized "+Pizza+" and with "+Topping+" as extra toppings.\n"+
                    "The total price is "+Sum+",-");
        /*This if statement differentiates between the two choices in the topping loop, the customer either orders
        a topping or chooses to finish the order at that point.*/
    }
}
